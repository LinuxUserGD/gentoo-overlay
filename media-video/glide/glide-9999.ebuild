# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

CRATES="
	addr2line@0.21.0
	adler@1.0.2
	aho-corasick@1.1.2
	android-tzdata@0.1.1
	android_system_properties@0.1.5
	anstream@0.6.4
	anstyle@1.0.4
	anstyle-parse@0.2.2
	anstyle-query@1.0.0
	anstyle-wincon@3.0.1
	anyhow@1.0.75
	atomic_refcell@0.1.13
	autocfg@1.1.0
	backtrace@0.3.69
	base64@0.21.4
	bitflags@1.3.2
	bitflags@2.4.0
	block-buffer@0.10.4
	bumpalo@3.14.0
	bytes@1.5.0
	cairo-rs@0.18.2
	cairo-sys-rs@0.18.2
	cc@1.0.83
	cfg-expr@0.15.5
	cfg-if@1.0.0
	chrono@0.4.31
	clap@4.4.6
	clap_builder@4.4.6
	clap_derive@4.4.2
	clap_lex@0.5.1
	colorchoice@1.0.0
	console@0.15.7
	core-foundation@0.9.3
	core-foundation-sys@0.8.4
	cpufeatures@0.2.9
	crypto-common@0.1.6
	digest@0.10.7
	directories@4.0.1
	dirs-sys@0.3.7
	either@1.9.0
	encode_unicode@0.3.6
	encoding_rs@0.8.33
	equivalent@1.0.1
	errno@0.3.5
	fastrand@2.0.1
	field-offset@0.3.6
	fnv@1.0.7
	foreign-types@0.3.2
	foreign-types-shared@0.1.1
	form_urlencoded@1.2.0
	futures-channel@0.3.28
	futures-core@0.3.28
	futures-executor@0.3.28
	futures-io@0.3.28
	futures-macro@0.3.28
	futures-sink@0.3.28
	futures-task@0.3.28
	futures-util@0.3.28
	gdk-pixbuf@0.18.0
	gdk-pixbuf-sys@0.18.0
	gdk4@0.7.3
	gdk4-sys@0.7.2
	gdk4-wayland@0.7.2
	gdk4-wayland-sys@0.7.2
	gdk4-win32@0.7.2
	gdk4-win32-sys@0.7.2
	gdk4-x11@0.7.2
	gdk4-x11-sys@0.7.2
	generic-array@0.14.7
	getrandom@0.2.10
	gimli@0.28.0
	gio@0.18.2
	gio-sys@0.18.1
	glib@0.18.2
	glib-macros@0.18.2
	glib-sys@0.18.1
	gobject-sys@0.18.0
	graphene-rs@0.18.1
	graphene-sys@0.18.1
	gsk4@0.7.3
	gsk4-sys@0.7.3
	gst-plugin-gtk4@0.11.1
	gst-plugin-version-helper@0.8.0
	gstreamer@0.21.1
	gstreamer-base@0.21.0
	gstreamer-base-sys@0.21.1
	gstreamer-gl@0.21.1
	gstreamer-gl-egl@0.21.1
	gstreamer-gl-egl-sys@0.21.1
	gstreamer-gl-sys@0.21.1
	gstreamer-gl-wayland@0.21.1
	gstreamer-gl-wayland-sys@0.21.1
	gstreamer-gl-x11@0.21.1
	gstreamer-gl-x11-sys@0.21.1
	gstreamer-play@0.21.0
	gstreamer-play-sys@0.21.0
	gstreamer-sys@0.21.1
	gstreamer-video@0.21.1
	gstreamer-video-sys@0.21.1
	gtk4@0.7.3
	gtk4-macros@0.7.2
	gtk4-sys@0.7.3
	h2@0.3.21
	hashbrown@0.12.3
	hashbrown@0.14.1
	heck@0.4.1
	hermit-abi@0.3.3
	http@0.2.9
	http-body@0.4.5
	httparse@1.8.0
	httpdate@1.0.3
	hyper@0.14.27
	hyper-tls@0.5.0
	iana-time-zone@0.1.57
	iana-time-zone-haiku@0.1.2
	idna@0.4.0
	indexmap@1.9.3
	indexmap@2.0.2
	indicatif@0.16.2
	ipnet@2.8.0
	itertools@0.11.0
	itoa@1.0.9
	js-sys@0.3.64
	lazy_static@1.4.0
	libadwaita@0.5.3
	libadwaita-sys@0.5.3
	libc@0.2.149
	linux-raw-sys@0.4.10
	log@0.4.20
	memchr@2.6.4
	memoffset@0.9.0
	mime@0.3.17
	miniz_oxide@0.7.1
	mio@0.8.8
	muldiv@1.0.1
	native-tls@0.2.11
	num-integer@0.1.45
	num-rational@0.4.1
	num-traits@0.2.17
	num_cpus@1.16.0
	number_prefix@0.4.0
	object@0.32.1
	once_cell@1.18.0
	openssl@0.10.57
	openssl-macros@0.1.1
	openssl-probe@0.1.5
	openssl-sys@0.9.93
	option-operations@0.5.0
	pango@0.18.0
	pango-sys@0.18.0
	paste@1.0.14
	percent-encoding@2.3.0
	pest@2.7.4
	pin-project-lite@0.2.13
	pin-utils@0.1.0
	pkg-config@0.3.27
	pretty-hex@0.3.0
	proc-macro-crate@1.3.1
	proc-macro-error@1.0.4
	proc-macro-error-attr@1.0.4
	proc-macro2@1.0.69
	quick-xml@0.20.0
	quote@1.0.33
	redox_syscall@0.2.16
	redox_syscall@0.3.5
	redox_users@0.4.3
	regex@1.10.1
	regex-automata@0.4.2
	regex-syntax@0.8.2
	reqwest@0.11.22
	rustc-demangle@0.1.23
	rustc_version@0.4.0
	rustix@0.38.19
	ryu@1.0.15
	schannel@0.1.22
	security-framework@2.9.2
	security-framework-sys@2.9.1
	self_update@0.28.0
	semver@0.11.0
	semver@1.0.20
	semver-parser@0.10.2
	serde@1.0.189
	serde_derive@1.0.189
	serde_json@1.0.107
	serde_spanned@0.6.3
	serde_urlencoded@0.7.1
	sha2@0.10.8
	slab@0.4.9
	smallvec@1.11.1
	socket2@0.4.9
	socket2@0.5.4
	strsim@0.10.0
	syn@1.0.109
	syn@2.0.38
	system-configuration@0.5.1
	system-configuration-sys@0.5.0
	system-deps@6.1.2
	target-lexicon@0.12.11
	tempfile@3.8.0
	thiserror@1.0.49
	thiserror-impl@1.0.49
	tinyvec@1.6.0
	tinyvec_macros@0.1.1
	tokio@1.33.0
	tokio-native-tls@0.3.1
	tokio-util@0.7.9
	toml@0.8.2
	toml_datetime@0.6.3
	toml_edit@0.19.15
	toml_edit@0.20.2
	tower-service@0.3.2
	tracing@0.1.39
	tracing-core@0.1.32
	try-lock@0.2.4
	typenum@1.17.0
	ucd-trie@0.1.6
	unicode-bidi@0.3.13
	unicode-ident@1.0.12
	unicode-normalization@0.1.22
	url@2.4.1
	utf8parse@0.2.1
	vcpkg@0.2.15
	version-compare@0.1.1
	version_check@0.9.4
	want@0.3.1
	wasi@0.11.0+wasi-snapshot-preview1
	wasm-bindgen@0.2.87
	wasm-bindgen-backend@0.2.87
	wasm-bindgen-futures@0.4.37
	wasm-bindgen-macro@0.2.87
	wasm-bindgen-macro-support@0.2.87
	wasm-bindgen-shared@0.2.87
	web-sys@0.3.64
	winapi@0.3.9
	winapi-i686-pc-windows-gnu@0.4.0
	winapi-x86_64-pc-windows-gnu@0.4.0
	windows@0.48.0
	windows-sys@0.45.0
	windows-sys@0.48.0
	windows-targets@0.42.2
	windows-targets@0.48.5
	windows_aarch64_gnullvm@0.42.2
	windows_aarch64_gnullvm@0.48.5
	windows_aarch64_msvc@0.42.2
	windows_aarch64_msvc@0.48.5
	windows_i686_gnu@0.42.2
	windows_i686_gnu@0.48.5
	windows_i686_msvc@0.42.2
	windows_i686_msvc@0.48.5
	windows_x86_64_gnu@0.42.2
	windows_x86_64_gnu@0.48.5
	windows_x86_64_gnullvm@0.42.2
	windows_x86_64_gnullvm@0.48.5
	windows_x86_64_msvc@0.42.2
	windows_x86_64_msvc@0.48.5
	winnow@0.5.17
	winreg@0.50.0
"

inherit cargo gnome2-utils meson xdg

DESCRIPTION="Cross-platform media player based on GStreamer and GTK+"
HOMEPAGE="https://github.com/philn/glide"

if [[ ${PV} == 9999 ]]
then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/philn/${PN}.git"
	RESTRICT="network-sandbox"
	KEYWORDS=""
else
	SRC_URI="https://github.com/philn/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz
		$(cargo_crate_uris)"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="0BSD Apache-2.0 Apache-2.0-with-LLVM-exceptions BSD Boost-1.0 MIT MPL-2.0 Unicode-DFS-2016 Unlicense ZLIB"
SLOT="0"
IUSE="egl wayland X"

DEPEND="
	dev-libs/glib:2
	gui-libs/gtk:4[X?,wayland?]
	gui-libs/libadwaita:1
	media-libs/gstreamer:1.0
	media-libs/gst-plugins-bad:1.0
	media-libs/gst-plugins-base:1.0
	media-plugins/gst-plugins-gtk:1.0[X?,egl?,wayland?]
"
RDEPEND="${DEPEND}"

QA_FLAGS_IGNORED="usr/bin/${PN}"

src_prepare() {
	default

	sed -i -e '/self-updater.*self_update/d' \
		Cargo.toml || die
	sed -i -e 's/\(Icon=.*\)\.svg/\1/' \
		data/net.baseart.Glide.desktop || die
	sed -i \
		-e '/cargo_env/d' \
		-e "$(usex !egl '/cargo_options/ s:x11egl::' '')" \
		-e "$(usex !wayland '/cargo_options/ s:wayland::' '')" \
		-e "$(usex !X '/cargo_options/ s:x11glx::' '')" \
		meson.build || die
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}
