# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..12} )

inherit cmake flag-o-matic optfeature python-any-r1 toolchain-funcs xdg

DESCRIPTION="Official desktop client for Telegram"
HOMEPAGE="https://desktop.telegram.org"

if [[ ${PV} == 9999 ]]
then
	inherit git-r3

	EGIT_BRANCH="dev"
	EGIT_REPO_URI="https://github.com/telegramdesktop/tdesktop.git"
	EGIT_SUBMODULES=(
		'*'
		'-Telegram/ThirdParty/hunspell'
		'-Telegram/ThirdParty/libdbusmenu-qt'
		'-Telegram/ThirdParty/lz4'
	)

	KEYWORDS=""
else
	MY_PN="tdesktop"
	MY_P="${MY_PN}-${PV}-full"

	LIBSRTP_VER="2.5.0"
	LIBYUV_COMMIT="04821d1e7d60845525e8db55c7bcd41ef5be9406"
	TG_OWT_COMMIT="afd9d5d31798d3eacf9ed6c30601e91d0f1e4d60"

	SRC_URI="
		https://github.com/telegramdesktop/${MY_PN}/releases/download/v${PV}/${MY_P}.tar.gz
		https://github.com/cisco/libsrtp/archive/refs/tags/v${LIBSRTP_VER}.tar.gz -> libsrtp-${LIBSRTP_VER}.tar.gz
		https://github.com/desktop-app/tg_owt/archive/${TG_OWT_COMMIT}.tar.gz -> tg_owt-${TG_OWT_COMMIT::7}.tar.gz
		https://gitlab.com/chromiumsrc/libyuv/-/archive/${LIBYUV_COMMIT}/libyuv-${LIBYUV_COMMIT}.tar.gz -> libyuv-${LIBYUV_COMMIT::7}.tar.gz
	"

	KEYWORDS="~amd64 ~x86"
	S="${WORKDIR}/${MY_P}"
fi

LICENSE="BSD GPL-3-with-openssl-exception LGPL-2+"
SLOT="0"
IUSE="alsa crashreporter custom-api-id enchant +fonts screencast wayland webkit"

CDEPEND="
	!net-im/telegram-desktop-bin
	app-arch/lz4:=
	>=dev-cpp/glibmm-2.77:2.68
	>=dev-cpp/abseil-cpp-20220623.1:=
	dev-libs/boost:=
	dev-libs/crc32c
	dev-libs/glib:2
	dev-libs/libfmt:=
	dev-libs/openssl:=
	dev-libs/protobuf:=
	>=dev-qt/qtbase-6.5:6=[dbus,gui,network,opengl,wayland?,widgets]
	>=dev-qt/qtimageformats-6.5:6
	>=dev-qt/qtsvg-6.5:6
	media-libs/fontconfig:=
	media-libs/libjpeg-turbo:=
	media-libs/libpulse
	>=media-libs/libvpx-1.10.0:=
	media-libs/openal
	media-libs/openh264:=
	media-libs/opus:=
	media-libs/rnnoise
	media-video/ffmpeg:=[opus,vpx]
	sys-libs/zlib:=[minizip]
	alsa? ( media-libs/alsa-lib )
	crashreporter? ( dev-util/google-breakpad )
	enchant? ( app-text/enchant:= )
	!enchant? ( >=app-text/hunspell-1.7:= )
	screencast? ( media-video/pipewire:= )
	webkit? (
		>=dev-qt/qtdeclarative-6.5:6
		>=dev-qt/qtwayland-6.5:6[compositor]
	)
"
RDEPEND="${CDEPEND}
	webkit? ( net-libs/webkit-gtk:4 )
"
DEPEND="${CDEPEND}
	screencast? (
		media-libs/libglvnd
		media-libs/mesa
		x11-libs/libdrm
	)
"
BDEPEND="
	${PYTHON_DEPS}
	>=dev-build/cmake-3.16
	dev-util/gdbus-codegen
	virtual/pkgconfig
	wayland? ( dev-util/wayland-scanner )
"

TG_OWT_DIR="${WORKDIR}/tg_owt"

# Current desktop-file-utils-0.26 does not understand Version=1.5
QA_DESKTOP_FILE="usr/share/applications/${PN}.desktop"

PATCHES=(
	"${FILESDIR}"/tdesktop-4.15.2-fix-clang.patch
)

pkg_pretend() {
	if use custom-api-id
	then
		[[ -n "${TDESKTOP_API_ID}" ]] && \
		[[ -n "${TDESKTOP_API_HASH}" ]] && (
			einfo "Will be used custom 'api_id' and 'api_hash':"
			einfo "TDESKTOP_API_ID=${TDESKTOP_API_ID}"
			einfo "TDESKTOP_API_HASH=${TDESKTOP_API_HASH//[!\*]/*}"
		) || (
			eerror "It seems you did not set one or both of"
			eerror "TDESKTOP_API_ID and TDESKTOP_API_HASH variables,"
			eerror "which are required for custom-api-id USE-flag."
			eerror "You can set them either in your env or bashrc."
			die
		)

		echo ${TDESKTOP_API_ID} | grep -q "^[0-9]\+$" || (
			eerror "Please check your TDESKTOP_API_ID variable"
			eerror "It should consist of decimal numbers only"
			die
		)

		echo ${TDESKTOP_API_HASH} | grep -q "^[0-9A-Fa-f]\{32\}$" || (
			eerror "Please check your TDESKTOP_API_HASH variable"
			eerror "It should consist of 32 hex numbers only"
			die
		)
	fi

	if tc-is-gcc && [[ $(gcc-major-version) -lt 7 ]]
	then
		die "At least gcc 7.0 is required"
	fi
}

git_unpack() {
	git-r3_src_unpack

	unset EGIT_BRANCH
	unset EGIT_SUBMODULES

	EGIT_COMMIT_DATE=$(GIT_DIR="${S}/.git" git show -s --format=%ct || die)

	EGIT_REPO_URI="https://github.com/desktop-app/tg_owt.git"
	EGIT_CHECKOUT_DIR="${TG_OWT_DIR}"

	git-r3_src_unpack
}

src_verify() {
	local git_name="$1"
	local var_name="$2"

	local commit=$(
		cat "${MY_P}"/Telegram/build/docker/centos_env/Dockerfile \
			| sed '/&& git.*'"${git_name}"'/,/&& git.*fetch/!d' \
			| tail -n1 | sed 's/.*\([0-9a-zA-Z]\{40\}\).*/\1/'
	)

	if [[ "${commit}" != "${!var_name}" ]]
	then
		die "You should update ${var_name}= to ${commit}"
	fi
}

src_unpack() {
	if [[ ${PV} == 9999 ]]
	then
		git_unpack
		return
	fi

	unpack "${MY_P}.tar.gz"

	src_verify tg_owt TG_OWT_COMMIT

	unpack "tg_owt-${TG_OWT_COMMIT::7}.tar.gz"
	mv "tg_owt-${TG_OWT_COMMIT}" "tg_owt" || die

	cd "tg_owt/src/third_party" || die

	unpack "libsrtp-${LIBSRTP_VER}.tar.gz"
	rmdir "libsrtp" || die
	mv "libsrtp-${LIBSRTP_VER}" "libsrtp" || die

	unpack "libyuv-${LIBYUV_COMMIT::7}.tar.gz"
	rmdir "libyuv" || die
	mv "libyuv-${LIBYUV_COMMIT}" "libyuv" || die
}

tg_owt_prepare() {
	local PATCHES=( "${TG_OWT_PATCHES[@]}" )

	pushd "${TG_OWT_DIR}" >/dev/null || die

	sed -i \
		-e '/include.*libabsl/d' \
		-e '/include.*libcrc32c/d' \
		-e '/include.*libopenh264/d' \
		CMakeLists.txt || die

	BUILD_DIR="${WORKDIR}/tg_owt_build" CMAKE_USE_DIR="${TG_OWT_DIR}" \
		cmake_src_prepare

	popd >/dev/null || die
}

src_prepare() {
	tg_owt_prepare

	cp "${FILESDIR}"/breakpad.cmake \
		cmake/external/crash_reports/breakpad/CMakeLists.txt || die

	sed -i -e 's:DESKTOP_APP_USE_PACKAGED:False:' \
		cmake/external/{expected,gsl,kcoreaddons,ranges,variant,xxhash}/CMakeLists.txt \
		Telegram/cmake/lib_tgvoip.cmake || die

	sed -i -e 's:find_package.*tg_owt:\0 PATHS ${libs_loc}/tg_owt_build:' \
		cmake/external/webrtc/CMakeLists.txt || die

	sed -i -e '/get_filename_component.*libs_loc/ s:/Libraries::' \
		cmake/variables.cmake || die

	if ! use webkit
	then
		sed -i -e '/find_package.*OPTIONAL_COMPONENTS/d' \
			Telegram/lib_webview/CMakeLists.txt || die
	fi

	# TDESKTOP_API_{ID,HASH} related:

	sed -i -e 's:if.*TDESKTOP_API_[A-Z]*.*:if(False):' \
		Telegram/cmake/telegram_options.cmake || die

	sed -i -e '/TDESKTOP_API_[A-Z]*/d' \
		Telegram/CMakeLists.txt || die

	if use custom-api-id
	then
		local -A api_defs=(
			[ID]="#define TDESKTOP_API_ID ${TDESKTOP_API_ID}"
			[HASH]="#define TDESKTOP_API_HASH ${TDESKTOP_API_HASH}"
		)
	else
		local -A api_defs=(
			[ID]=$(
				cat snap/snapcraft.yaml \
					| ( grep TDESKTOP_API_ID || die ) \
					| sed 's:.*=\(.*\):#define TDESKTOP_API_ID \1:'
			)

			[HASH]=$(
				cat snap/snapcraft.yaml \
					| ( grep TDESKTOP_API_HASH || die ) \
					| sed 's:.*=\(.*\):#define TDESKTOP_API_HASH \1:'
			)
		)
	fi

	sed -i \
		-e "/#if.*defined.*TDESKTOP_API_ID/i ${api_defs[ID]}" \
		-e "/#if.*defined.*TDESKTOP_API_HASH/i ${api_defs[HASH]}" \
		Telegram/SourceFiles/config.h || die

	cmake_src_prepare
}

tg_owt_configure() {
	local mycmakeargs=(
		-DBUILD_SHARED_LIBS=OFF
		-DTG_OWT_BUILD_AUDIO_BACKENDS=OFF
		-DTG_OWT_USE_X11=OFF

		-DTG_OWT_USE_PIPEWIRE=$(usex screencast)
	)

	BUILD_DIR="${WORKDIR}/tg_owt_build" CMAKE_USE_DIR="${TG_OWT_DIR}" \
		cmake_src_configure
}

src_configure() {
	local mycxxflags=(
		# Fix ld error with the latest dev-cpp/abseil-cpp
		'-DNDEBUG'

		-Wno-deprecated
		-Wno-deprecated-declarations

		# Supress QA Notice
		-Wno-array-bounds
		-Wno-free-nonheap-object
		-Wno-uninitialized
	)

	append-cxxflags ${mycxxflags[@]}

	tg_owt_configure

	local mycmakeargs=(
		-DCMAKE_DISABLE_PRECOMPILE_HEADERS=OFF

		-DDESKTOP_APP_DISABLE_X11_INTEGRATION=ON
		-DDESKTOP_APP_USE_PACKAGED=ON
		-DDESKTOP_APP_USE_PACKAGED_RLOTTIE=OFF
		-DLIBTGVOIP_DISABLE_PULSEAUDIO=OFF
		-DQT_VERSION_MAJOR=6

		-DDESKTOP_APP_DISABLE_CRASH_REPORTS=$(usex !crashreporter)
		-DDESKTOP_APP_DISABLE_WAYLAND_INTEGRATION=$(usex !wayland)
		-DDESKTOP_APP_USE_ENCHANT=$(usex enchant)
		-DDESKTOP_APP_USE_PACKAGED_FONTS=$(usex !fonts)
		-DLIBTGVOIP_DISABLE_ALSA=$(usex !alsa)
	)

	cmake_src_configure
}

src_compile() {
	BUILD_DIR="${WORKDIR}/tg_owt_build" CMAKE_USE_DIR="${TG_OWT_DIR}" \
		cmake_src_compile

	cmake_src_compile
}

pkg_postinst() {
	xdg_pkg_postinst
	optfeature_header
	optfeature "shop payment support" net-libs/webkit-gtk:4
}
